#include <stdio.h>

int main() {
    int x, i;

    printf("Enter a Number \n");
    scanf("%d", &x);

    printf("\nThe factors of the %d are: ", x);
    for (i = 1; i <= x; ++i) {
        if (x % i == 0) {
            printf("%d ", i);
        }
    }
    printf("\n");

    getch();
    return 0;
}
